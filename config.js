module.exports = {
  secret: 'EXAMPLEDONTUSE',
  API_ENDPOINT: '/api',
  theme: 'PepperTheme',
  editor: 'app/components/BookWriter/BookWriter.jsx',
  routes: 'app/routes.jsx',
  navigation: 'app/components/Navigation/Navigation.jsx'
}
