var React = require('react')
var ReactDOM = require('react-dom')
// var LensWriter = require('lens/LensWriter')
var BookEditorPackage = require('book-editor/BookEditorPackage')
var BookEditor = require('book-editor/BookEditor')
var Configurator = require('book-editor/node_modules/substance/util/Configurator')
var Component = require('book-editor/node_modules/substance/ui/Component')
var DocumentSession = require('book-editor/node_modules/substance/model/DocumentSession')

// const LensArticleExporter = require('lens/model/LensArticleExporter')
const BookChapterImporter = require('./BookChapterImporter')
const HTMLImporter = require('pubsweet-core/node_modules/substance/model/HTMLImporter')
// const LensArticleImporter = require('lens/model/LensArticleImporter')

// const defaultLensArticle = require('lens/model/defaultLensArticle')

// BooksWriter wrapped in a React component
// ------------------

class ReactBookWriter extends React.Component {

  getWriter () {
    return this
  }

  createDocumentSession () {
    var content = this.props.content ? this.props.content : [
      /* '<article xmlns="http://substance.io/science-article/0.1.0" lang="en">',
      '<meta><title>Enter title</title><abstract>Enter abstract</abstract></meta>',
      '<resources></resources>',
      '<body><p id="p1">Enter your article here.</p></body>',
      '</article>' */
      '<body><p>Enter the contents here.</p></body>'
    ].join('')

    var format = this.props.format ? this.props.format : 'xml'
    var doc = this.createDoc(content, format)
    var documentSession = new DocumentSession(doc)

    return documentSession
  }

  // New props arrived, update the editor
  componentDidUpdate () {
    var documentSession = this.createDocumentSession()

    this.writer.extendProps({
      documentSession: documentSession
    })
  }

  createDoc (content, format) {
    var doc
    var importer
    switch (format) {
      case 'xml':
        importer = new BookChapterImporter()
        doc = importer.importDocument(content)
        break
      default:
        importer = new HTMLImporter()
        doc = importer.importDocument(content)
        break
    }
    return doc
  }

  save (source, changes, callback) {
    // var exporter = new LensArticleExporter()
    // this.props.onSave(exporter.exportDocument(source), callback)
    console.log('do nothing for now')
  }

  componentDidMount () {
    var el = ReactDOM.findDOMNode(this)
    var documentSession = this.createDocumentSession()
    var configurator = new Configurator({
      name: 'book-example',
      configure: function (config) {
        config.import(BookEditorPackage)
      }
    })

    this.writer = Component.mount(BookEditor, {
      documentSession: documentSession,
      onSave: this.save.bind(this),
      onUploadFile: this.props.onUploadFile,
      configurator: configurator
    }, el)
  }

  componentWillUnmount () {
    this.writer.dispose()
  }

  render () {
    return React.DOM.div({
      className: 'book-writer-wrapper'
    })
  }
}

ReactBookWriter.propTypes = {
  onSave: React.PropTypes.func,
  onUploadFile: React.PropTypes.func,
  format: React.PropTypes.string,
  documentId: React.PropTypes.string,
  content: React.PropTypes.object,
  version: React.PropTypes.number
}

module.exports = ReactBookWriter
