var schema = require('./chapterSchema')
var _ = require('book-editor/node_modules/substance/util/helpers') // ugly since _ is widely known for lodash or underscore
var Document = require('book-editor/node_modules/substance/model/Document')
var DocumentIndex = require('book-editor/node_modules/substance/model/DocumentIndex')

/* var Bibliography = require('../packages/bibliography/Bibliography')
var Collection = require('./Collection')*/

var without = require('pubsweet-core/node_modules/lodash/without')

var BookChapter = function () {
  // HACK: at the moment we need to seed this way
  // as containers get registered on construction
  // it would be better if we created the containers dynamically
  BookChapter.super.call(this, schema)

  this.create({
    type: 'container',
    id: 'main',
    nodes: []
  })

  this.collections = {}

  this.includesIndex = this.addIndex('includes', DocumentIndex.create({
    type: 'include',
    property: 'nodeId'
  }))

  this.citationsIndex = this.addIndex('citations', DocumentIndex.create({
    type: 'citation',
    property: 'targets'
  }))

  this.connect(this, {
    'document:changed': this.onDocumentChanged
  })
}

BookChapter.Prototype = function () {
  // HACK: We ensure referential integrity by just patching the targets property
  // of citation nodes until we have a better solution:
  // See: https://github.com/book-editor/node_modules/substance//book-editor/node_modules/substance//issues/295
  this.onDocumentChanged = function (change) {
    _.each(change.ops, function (op) {
      if (op.isDelete()) {
        var deletedNode = op.val

        // Check if deleted node is a citeable node
        // TODO: check if this is necessary
        if (deletedNode.type === 'image-figure' || deletedNode.type === 'table-figure' || deletedNode.type === 'bib-item') {
          var citations = this.getIndex('citations').get(deletedNode.id)
          _.each(citations, function (citation) {
            citation.targets = without(citation.targets, deletedNode.id)
          })
        }
      }
    }.bind(this))
  }

  this.updateCollections = function () {
    _.each(this.collections, function (c) {
      c.update()
    })
  }

  this.getDocumentMeta = function () {
    return this.get('article-meta')
  }

  this.getCiteprocCompiler = function () {
    return this.citeprocCompiler
  }

  this.getBibliography = function () {
    return this.collections['bib-item']
  }

  this.getCollection = function (itemType) {
    return this.collections[itemType]
  }

  // Document title
  this.getTitle = function () {
    return this.get('article-meta').title
  }
}

Document.extend(BookChapter)

BookChapter.XML_TEMPLATE = [
  '<body>',
  '<p id=\'p1\'>Enter your article here.</p>',
  '</body>'
].join('')

module.exports = BookChapter
