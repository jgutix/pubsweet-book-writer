var DocumentSchema = require('book-editor/node_modules/substance/model/DocumentSchema')

// pubsweet-core/node_modules/substance Node Types
// ----------------------

var Paragraph = require('book-editor/node_modules/substance/packages/paragraph/Paragraph')
var Heading = require('book-editor/node_modules/substance/packages/heading/Heading')
var Codeblock = require('book-editor/node_modules/substance/packages/codeblock/Codeblock')
var Blockquote = require('book-editor/node_modules/substance/packages/blockquote/Blockquote')
var Image = require('book-editor/node_modules/substance/packages/image/Image')
var ListItem = require('book-editor/node_modules/substance/packages/list/ListItem')
var Emphasis = require('book-editor/node_modules/substance/packages/emphasis/Emphasis')
var Strong = require('book-editor/node_modules/substance/packages/strong/Strong')
var Subscript = require('book-editor/node_modules/substance/packages/subscript/Subscript')
var Superscript = require('book-editor/node_modules/substance/packages/superscript/Superscript')
var Code = require('book-editor/node_modules/substance/packages/code/Code')
var Link = require('book-editor/node_modules/substance/packages/link/Link')

var schema = new DocumentSchema('lens-article', '3.0.0')

schema.getDefaultTextType = function () {
  return 'paragraph'
}

schema.addNodes([
  Paragraph, Heading,
  Codeblock,
  Blockquote,
  Code, Emphasis, Strong, Subscript, Superscript,
  Link,
  Image,
  ListItem
])

module.exports = schema
