'use strict'

var XMLImporter = require('book-editor/node_modules/substance/model/XMLImporter')
var chapterSchema = require('./chapterSchema')
var BookChapter = require('./BookChapter')

var converters = [
  require('book-editor/node_modules/substance/packages/paragraph/ParagraphHTMLConverter'),
  require('book-editor/node_modules/substance/packages/blockquote/BlockquoteHTMLConverter'),
  require('book-editor/node_modules/substance/packages/codeblock/CodeblockHTMLConverter'),
  require('book-editor/node_modules/substance/packages/heading/HeadingHTMLConverter'),
  require('book-editor/node_modules/substance/packages/image/ImageXMLConverter'),
  require('book-editor/node_modules/substance/packages/strong/StrongHTMLConverter'),
  require('book-editor/node_modules/substance/packages/emphasis/EmphasisHTMLConverter'),
  require('book-editor/node_modules/substance/packages/link/LinkHTMLConverter')
]

function BookChapterImporter () {
  XMLImporter.call(this, {
    schema: chapterSchema,
    converters: converters,
    DocumentClass: BookChapter
  })
}

BookChapterImporter.Prototype = function () {
  this.convertDocument = function (articleElement) {
    // Import main container
    var bodyNodes = articleElement.children
    this.convertContainer(bodyNodes, 'body')
  }
}

// Expose converters so we can reuse them in NoteHtmlExporter
BookChapterImporter.converters = converters

XMLImporter.extend(BookChapterImporter)

module.exports = BookChapterImporter
