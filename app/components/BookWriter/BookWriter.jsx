import React from 'react'
import ReactBookWriter from './ReactBookWriter'

// Styles
import './BookWriter.scss'

class BookWriter extends React.Component {
  constructor (props) {
    super(props)
    this.save = this.save.bind(this)
    this.uploadFile = this.uploadFile.bind(this)
  }

  save (source, callback) {
    let doc = Object.assign(this.props.fragment, {
      source: source
    })
    this.props.save(doc)
    callback(null, source)
  }

  uploadFile (file, callback) {
    return this.props.uploadFile(file, callback)
  }

  render () {
    return <ReactBookWriter
      // documentId={this.props.fragment.id}
      // version={this.props.fragment.version}
      // content={this.props.fragment.source}
      onSave={this.save}
      format='xml'
      onUploadFile={this.uploadFile}
      />
  }
}

BookWriter.propTypes = {
  fragment: React.PropTypes.object,
  save: React.PropTypes.func,
  uploadFile: React.PropTypes.func
}

export default BookWriter
