import React from 'react'
import { Route } from 'react-router'

import { requireAuthentication } from 'pubsweet-core/app/components/AuthenticatedComponent'

// Manage
import Manage from 'pubsweet-core/app/components/Manage/Manage'
import PostsManager from 'pubsweet-core/app/components/PostsManager/PostsManager'
// import ScienceWriter from 'pubsweet-core/app/components/ScienceWriter/ScienceWriter'
import BookWriter from '../app/components/BookWriter/BookWriter'
import UsersManager from 'pubsweet-core/app/components/UsersManager/UsersManager'

// Public
import Blog from 'pubsweet-core/app/components/Blog/Blog'
// import ScienceReader from 'pubsweet-core/app/components/ScienceReader/ScienceReader'

// Authentication
import Login from 'pubsweet-core/app/components/Login/Login'
import Signup from 'pubsweet-core/app/components/Signup/Signup'

export default (
  <Route>
    <Route path='/' component={Blog}/>

    <Route path='/manage' component={requireAuthentication(Manage)}>
      <Route path='posts' component={PostsManager} />
      <Route path='sciencewriter/:id' component={BookWriter} />
      <Route path='users' component={UsersManager} />
    </Route>

    <Route path='/login' component={Login} />
    <Route path='/signup' component={Signup} />
  </Route>
)
