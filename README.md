# Book Writer

Powered by PubSweet.

# Preconditions

Node.js, version must be of 4.x.x series, is a requirement, read about how to install it here: [https://nodejs.org/en/](https://nodejs.org/en/)

Warning about `npm`: Because the newly released npm 3 changed the way npm installs nested dependencies (it flattens them), and we rely on previous behaviour (https://github.com/npm/npm/issues/9809) please use npm 2.x while we work on resolving this.

# Install

```bash
$ git clone git@gitlab.coko.foundation:pubsweet/science-blogger.git
$ cd science-blogger
$ npm install
```

# Start the server

First, initialize your blog by running and going through the setup process:
```bash
$ NODE_ENV=dev npm run setup
```

To start the JS compilation and webserver, run:
```bash
$ npm run dev
```

Point your browser to: [http://localhost:3000/manage/posts](http://localhost:3000/manage/posts) and login with the chosen admin username and password and all should be well. Visit [http://localhost:3000](http://localhost:3000) for the blog landing page.

# Themes

Themes are a PubSweet component. If you want to write a custom theme, set your theme component in `config.js`. When you require a style from a component, using e.g. `import './Signup.scss'` in `app/components/Signup/Signup.jsx`, we'll automatically find the right themed style (e.g. `app/components/PepperTheme/Signup/Signup.scss` if theme is set to `PepperTheme`). You can then continue working on your themed styles as usual, and the page will hot-reload when you change anything.

# How to look into the database for debugging purposes

Run a PouchDB server (comes with the app):
```bash
$ npm run pouchdb
```

And navigate to [http://localhost:5984/_utils/](http://localhost:5984/_utils/). Click "Add New Database" and enter "dev", to connect to the development database. You should now be able to run queries on your development database.

# Production installation

These are instructions for Ubuntu 15.10, exact steps may vary from OS to OS so if you're using another system, please take this as general guidance only.

First [install node 4.x](https://github.com/nodesource/distributions#debinstall)

```bash
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Then clone the repository:

```bash
git clone https://gitlab.coko.foundation/pubsweet/science-blogger.git
```

Install the required npm modules:

```bash
npm install
```

Build the production JS:

```bash
npm run build
```

Configure your initial admin account:

```bash
NODE_ENV=production npm run setup
```

Start the server:

```bash
npm run start
```

The application should now be accessible through port 80 on your server.
